FROM node:lts-alpine

WORKDIR /app/es6
RUN cd ..

COPY ./package.json ./
RUN npm install

COPY . .

RUN npm run build
RUN npm run test

CMD [ "node", "./es5/main.js" ]