'use strict';

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var findTransmission = function findTransmission(bits) {
    var transmissionRate = 1;
    var currentSeqLen = 0;
    var prevBit = bits[0];
    var maxSeq = void 0;
    var minSeq = void 0;
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
        for (var _iterator = bits[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var bit = _step.value;

            if (bit === prevBit) {
                currentSeqLen++;
            } else {
                prevBit = bit;
                if (maxSeq === undefined) {
                    minSeq = currentSeqLen;
                    maxSeq = minSeq;
                } else {
                    maxSeq = currentSeqLen > maxSeq ? currentSeqLen : maxSeq;
                    minSeq = currentSeqLen < minSeq ? currentSeqLen : minSeq;
                }
                currentSeqLen = 1;
            }
        }
    } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
    } finally {
        try {
            if (!_iteratorNormalCompletion && _iterator.return) {
                _iterator.return();
            }
        } finally {
            if (_didIteratorError) {
                throw _iteratorError;
            }
        }
    }

    if (maxSeq !== undefined) {
        if (maxSeq === minSeq) {
            transmissionRate = maxSeq / timeStamps()['tiny'];
        } else if (maxSeq / minSeq === 3) {
            transmissionRate = maxSeq / timeStamps()['middle'];
        } else {
            transmissionRate = maxSeq / timeStamps()['large'];
        }
    } else {
        transmissionRate = currentSeqLen;
    }
    return transmissionRate;
};
var decodeBits = function decodeBits(bits) {
    var _jsonOfDotsAndDashes, _jsonOfSpaces;

    bits.replace('0', ' ');
    bits.trim;
    bits.replace(' ', '0');
    var spaceBit = '0';
    var timeUnit = findTransmission(bits);
    var jsonOfDotsAndDashes = (_jsonOfDotsAndDashes = {}, _defineProperty(_jsonOfDotsAndDashes, timeStamps()['tiny'], '·'), _defineProperty(_jsonOfDotsAndDashes, timeStamps()['middle'], '−'), _jsonOfDotsAndDashes);
    var jsonOfSpaces = (_jsonOfSpaces = {}, _defineProperty(_jsonOfSpaces, timeStamps()['middle'], ' '), _defineProperty(_jsonOfSpaces, timeStamps()['large'], '   '), _jsonOfSpaces);
    var finalMessage = '';
    if (bits === null || bits === undefined) {} else {
        var lastBit = bits[bits.length - 1];
        bits = bits + (lastBit === spaceBit ? '1' : '0');
        var prevBit = bits[0];
        var currentSeqLength = 0;
        var _iteratorNormalCompletion2 = true;
        var _didIteratorError2 = false;
        var _iteratorError2 = undefined;

        try {
            for (var _iterator2 = bits[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                var bit = _step2.value;

                if (bit !== prevBit) {
                    var spaceWord = jsonOfSpaces[currentSeqLength / timeUnit] === undefined ? '' : jsonOfSpaces[currentSeqLength / timeUnit];
                    var characterWord = jsonOfDotsAndDashes[currentSeqLength / timeUnit] === undefined ? '' : jsonOfDotsAndDashes[currentSeqLength / timeUnit];
                    finalMessage += prevBit === spaceBit ? spaceWord : characterWord;
                    currentSeqLength = 0;
                }
                currentSeqLength++;
                prevBit = bit;
            }
        } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                    _iterator2.return();
                }
            } finally {
                if (_didIteratorError2) {
                    throw _iteratorError2;
                }
            }
        }
    }
    return finalMessage;
};

var decodeMorse = function decodeMorse(morseCode) {
    var words = morseCode.split('   ');
    var finalSentence = '';
    var _iteratorNormalCompletion3 = true;
    var _didIteratorError3 = false;
    var _iteratorError3 = undefined;

    try {
        for (var _iterator3 = words[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
            var word = _step3.value;

            var letters = word.split(' ');
            var _iteratorNormalCompletion4 = true;
            var _didIteratorError4 = false;
            var _iteratorError4 = undefined;

            try {
                for (var _iterator4 = letters[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                    var letter = _step4.value;

                    finalSentence += morseDictionery[letter] === undefined ? '' : morseDictionery[letter];
                }
            } catch (err) {
                _didIteratorError4 = true;
                _iteratorError4 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion4 && _iterator4.return) {
                        _iterator4.return();
                    }
                } finally {
                    if (_didIteratorError4) {
                        throw _iteratorError4;
                    }
                }
            }

            finalSentence += ' ';
        }
    } catch (err) {
        _didIteratorError3 = true;
        _iteratorError3 = err;
    } finally {
        try {
            if (!_iteratorNormalCompletion3 && _iterator3.return) {
                _iterator3.return();
            }
        } finally {
            if (_didIteratorError3) {
                throw _iteratorError3;
            }
        }
    }

    return finalSentence.trim();
};

var morseDictionery = {
    '·−': 'A',
    '−···': 'B',
    '−·−·': 'C',
    '−··': 'D',
    '·': 'E',
    '··−·': 'F',
    '−−·': 'G',
    '····': 'H',
    '··': 'I',
    '·−−−': 'J',
    '−·−': 'K',
    '·−··': 'L',
    '−−': 'M',
    '−·': 'N',
    '−−−': 'O',
    '·−−·': 'P',
    '−−·−': 'Q',
    '·−·': 'R',
    '···': 'S',
    '−': 'T',
    '··−': 'U',
    '···−': 'V',
    '·−−': 'W',
    '−··−': 'X',
    '−·−−': 'Y',
    '−−··': 'Z',
    '·−−−−': '1',
    '··−−−': '2',
    '···−−': '3',
    '····−': '4',
    '·····': '5',
    '−····': '6',
    '−−···': '7',
    '−−−··': '8',
    '−−−−·': '9',
    '−−−−−': '0',
    '·−·−·−': '.'
};

var timeStamps = function timeStamps() {
    return { 'tiny': 1,
        'middle': 3,
        'large': 7 };
};

console.log('DOCKER CHECK');

module.exports = { decodeBits: decodeBits, decodeMorse: decodeMorse };