import * as functions from './es5/main.js';
const bitsMessage = '1100110011001100000011000000111111001100111111001111110000000000000011001111110011111100111111000000110011001111110000001111110011001100000011';
const bitsMessage1Rate = '10101010001000111010111011100000001011101110111000101011100011101010001';
const dotsMessage = '···· · −·−−   ·−−− ··− −·· ·';
const actualMessage = 'HEY JUDE';

describe('unit testing', ()=>{
    describe('tests bits decodation function', ()=>{

        test('bits to dots, 2 rate, no noise', () => {
            expect(functions.decodeBits(bitsMessage)).toBe(dotsMessage);
        });

        test('bits to dots, 2 rate, noise', () => {
            expect(functions.decodeBits('0'+bitsMessage+'00')).toBe(dotsMessage);
        });

        test('single bit', () => {
            expect(functions.decodeBits('11')).toBe('·');
        });
        
        test('bits to dots, 1 rate', () => {
            expect(functions.decodeBits(bitsMessage1Rate)).toBe(dotsMessage);
        });

        test('bits to dots but empty', () => {
            expect(functions.decodeBits('')).toBe('');
        });
    })

    describe('dots to morse tests', ()=>{
        test('dots to words', ()=> {
            expect(functions.decodeMorse(dotsMessage)).toBe(actualMessage);
        });
        
        test('dots to words but empty', ()=> {
            expect(functions.decodeMorse('')).toBe('');
        });
    });
});