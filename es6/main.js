const findTransmission=(bits)=>{
    let transmissionRate = 1;
    let currentSeqLen = 0;
    let prevBit = bits[0];
    let maxSeq;
    let minSeq;
    for(const bit of bits){
        if(bit === prevBit){
            currentSeqLen++;
        }
        else{
            prevBit = bit;
            if(maxSeq === undefined){
                minSeq = currentSeqLen;
                maxSeq = minSeq;
            }
            else{
                maxSeq = currentSeqLen>maxSeq?currentSeqLen:maxSeq;
                minSeq = currentSeqLen<minSeq?currentSeqLen:minSeq;
            }
            currentSeqLen = 1;
        }
    }
    if(maxSeq !== undefined){
        if(maxSeq === minSeq){
            transmissionRate = maxSeq/timeStamps()['tiny'];
        }
        else if(maxSeq/minSeq === 3){
            transmissionRate = maxSeq/timeStamps()['middle'];
        }
        else{
            transmissionRate = maxSeq/timeStamps()['large'];
        }
    }
    else{
        transmissionRate = currentSeqLen;
    }
    return transmissionRate;
}
const decodeBits=(bits)=>{
    bits.replace('0', ' ');
    bits.trim;
    bits.replace(' ', '0');
    const spaceBit = '0';
    const timeUnit = findTransmission(bits);
    const jsonOfDotsAndDashes={[timeStamps()['tiny']]:'·', [timeStamps()['middle']]:'−'};
    const jsonOfSpaces = {[timeStamps()['middle']]:' ', [timeStamps()['large']]:'   '};
    let finalMessage = '';
    if(bits === null || bits === undefined){}
    else{
        const lastBit = bits[bits.length-1];
        bits = bits+ (lastBit === spaceBit?'1':'0');
        let prevBit = bits[0];
        let currentSeqLength = 0;
        for(const bit of bits){
            if(bit !== prevBit){
                const spaceWord = jsonOfSpaces[currentSeqLength/timeUnit] === undefined?'':
                jsonOfSpaces[currentSeqLength/timeUnit];
                const characterWord = jsonOfDotsAndDashes[currentSeqLength/timeUnit] === undefined?'':
                jsonOfDotsAndDashes[currentSeqLength/timeUnit];
                finalMessage += prevBit === spaceBit?spaceWord:
                characterWord;
                currentSeqLength = 0;
            }
            currentSeqLength++;
            prevBit = bit;
        }
    }
    return finalMessage
}

const decodeMorse=(morseCode)=>{
    const words = morseCode.split('   ');
    let finalSentence = '';
    for(const word of words){
        const letters = word.split(' ');
        for(const letter of letters){
            finalSentence += morseDictionery[letter] === undefined?'':morseDictionery[letter];
        }
        finalSentence+=' ';
    }
    return finalSentence.trim();
}

const morseDictionery = { 
    '·−':     'A',
    '−···':   'B',
    '−·−·':   'C',
    '−··':    'D',
    '·':      'E',
    '··−·':   'F',
    '−−·':    'G',
    '····':   'H',
    '··':     'I',
    '·−−−':   'J',
    '−·−':    'K',
    '·−··':   'L',
    '−−':     'M',
    '−·':     'N',
    '−−−':    'O',
    '·−−·':   'P',
    '−−·−':   'Q',
    '·−·':    'R',
    '···':    'S',
    '−':      'T',
    '··−':    'U',
    '···−':   'V',
    '·−−':    'W',
    '−··−':   'X',
    '−·−−':   'Y',
    '−−··':   'Z',
    '·−−−−':  '1',
    '··−−−':  '2',
    '···−−':  '3',
    '····−':  '4',
    '·····':  '5',
    '−····':  '6',
    '−−···':  '7',
    '−−−··':  '8',
    '−−−−·':  '9',
    '−−−−−':  '0',
    '·−·−·−': '.'
};

const timeStamps=()=>{
    return{'tiny':1,
    'middle':3,
    'large':7};
}

console.log('DOCKER CHECK');

module.exports = {decodeBits, decodeMorse};